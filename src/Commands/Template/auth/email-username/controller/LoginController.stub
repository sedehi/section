<?php

namespace {{{appName}}}Http\Controllers\Auth\Controllers\Site;

use {{{appName}}}Http\Controllers\Auth\Requests\Site\AuthRequest;
use {{{appName}}}Exceptions\UserBanned;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Validator;
use {{{appName}}}Http\Controllers\Controller;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    protected $captchaRequiredAttemptCount = 3;

    public function showLoginForm()
    {
        return view('Auth.views.site.login');
    }

    public function login(AuthRequest $request)
    {
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        $validatorMail = Validator::make($request->all(), [
            'email_or_username' => 'email'
        ]);

        if ($validatorMail->passes()) {
            $login = auth()->attempt([
                'email'    => strtolower($request->get('email_or_username')),
                'password' => $request->get('password')
            ], $request->has('remember'));
        } else {
            $login = auth()->attempt([
                'username'  => $request->get('email_or_username'),
                'password'  => $request->get('password')
            ], $request->has('remember'));
        }

        if ($login) {
            if (!is_null(auth()->user()->banned_at)) {
                throw new UserBanned('site');
            }

            $request->session()->regenerate();
            
            $this->clearLoginAttempts($request);

            session()->forget('captcha_required');

            if ($request->has('return')) {
                return redirect()->to($request->get('return'));
            }

            return redirect()->to(config('site.dashboard'));
        }

        $this->incrementLoginAttempts($request);

        $attempts_count = $this->limiter()->attempts($this->throttleKey($request));

        if ($attempts_count >= $this->captchaRequiredAttemptCount) {
            session()->put('captcha_required', 'required|');
        }

        return redirect()->action('Auth\Controllers\Site\LoginController@showLoginForm')
                        ->with('error', trans('auth.failed'));
    }

    public function logout()
    {
        auth()->logout();

        return redirect()->action('HomeController@index');
    }
}
